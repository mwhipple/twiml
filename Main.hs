-- Currently this file doesn't do anything useful but is used to validate
-- that data seems to be getting shuffled around properly

{-# LANGUAGE OverloadedStrings #-}

-- TODO: Allow streaming without full structure creation
-- Check incremental example in Data.Binary.Get
-- Move away from single model object into chunkier pieces
module Main (main) where

import Data.Aeson (encode)
import Data.Binary.Get
import qualified Data.ByteString.Lazy.Char8 as BL

import Twiml.Config
import Twiml.Config.YAML
import Twiml.Config.Binary

main = do
  input <- BL.readFile "twiddler.cfg"
  let config = (runGet getConfig input)
  print config
  BL.putStrLn (encode config)
