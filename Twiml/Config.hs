-----------------------------------------------------------------------------
-- |
-- Module      : Twiml.Config
-- Copyright   : (c) Matt Whipple, 2018
-- License     : MIT
--
-- Maintainer  : matt@mattwhipple.com
-- Stability   : experimental
-- Portability : non-portable (GHC extensions)
--
-- | Model for Twiddler Configuration
-- The model which serves as the intermediate representation
-- between parsing and rendering. The binary format ostensibly
-- represents the canonical format but the model here will
-- align with the YAML format as it is more friendly for
-- understanding and writing code against, and also allows
-- for greater boilerplate code reduction.
--
-- The data types, however, correspond to those in the binary format.
-- This avoids the need to perform some exta conversion while also
-- preserving the constraints associated with the underlying data format.
-----------------------------------------------------------------------------
module Twiml.Config
 ( Chord(..)
 , ChordMapping(..)
 , Config(..)
 , Event(..)
 , Header(..)
 , HIDCode(..)
 , HIDMod(..)
 , Note(..)
 , Options(..)
 , StringLocation
 , Thumb(..)
 ) where

-------------
-- Imports --
-------------

import Data.Maybe
  ( isJust
  , fromJust
  )

import Data.Word
  ( Word8
  , Word16
  , Word32
  )

----------
-- Text --
----------

-- | Top-level Config object
--
-- The final members for this type are expected to be
--  * header: scalar/fixed sized options
--  * chordMappings: chord->event mappings
-- TODO: Encapsulate String table stuff
data Config = Config
  { header          :: Header
  , chordMappings   :: [ChordMapping]
  , stringLocations :: [StringLocation]
  , stringContents  :: [[Event]]
  } deriving (Show)

-- | Configuration Header.
-- This is effectively where all of fixed size data gets placed.
-- Number of chords is specified in the header of the binary
-- format as it is required for parsing the file, but represents
-- a calculated value within a complete Config.
data Header = Header
  { version                 :: Word8
  , options                 :: Options
  , sleepTimeout            :: Word16
  , mouseLeftClickOrChord   :: Word16
  , mouseRightClickOrChord  :: Word16
  , mouseMiddleClickOrChord :: Word16
  , mouseAccelFactor        :: Word8
  , keyRepeatDelay          :: Word8
  } deriving (Show)

-- | The flags present in the Header.
-- In the binary format these are in bits
-- within one byte.
-- TODO: Merge into Header
data Options = Options
  { keyRepeatEnabled       :: Bool
  , directKeyModeEnabled   :: Bool
  , joystickMouseClickLeft :: Bool
  , bluetoothRadioDisabled :: Bool
  , stickNumKeyEnabled     :: Bool
  , stickShiftKeyEnabled   :: Bool
  } deriving (Show)

-----------------------------
-- Chord Definitions
-----------------------------

-- | A Chord
--
-- Consists of 0 or more Thumb modifiers
-- and the Note (or O) for each key row.
-- Each row can have at most one (0|1) Note
data Chord = Chord [Thumb] Note Note Note Note
  deriving (Show)

-- | A Thumb modfifier key.
-- Multiple modifiers can be active for a Chord
data Thumb = N    -- Num
           | A    -- Alt
           | C    -- Ctrl
           | S    -- Shft
  deriving (Show)

-- | A Note indicates which column should be pressed in a given row.
-- This uses the standard front/house perspective rather than the
-- potentially more useful back/stage perspective.
data Note = O   -- No key/Open (also looks like 0)
          | R   -- Right Column (A..)
          | M   -- Middle Column (E..)
          | L   -- Left Column (BS..)
  deriving (Show, Eq, Enum)

-- | A Modifier for an HID event.
data HIDMod = LC  -- Left Ctrl
            | LS  -- Left Shift
            | LA  -- Left Alt
            | LG  -- Left GUI
            | RC  -- Right Ctrl
            | RS  -- Right Shift
            | RA  -- Right Alt
            | RG  -- Right GUI
  deriving (Show)

symEventAlist :: [(Word8, String)]
symEventAlist =
  let letters = zip [4..] [[x] | x <- ['a'..'z']]
      motion =
        [ (44, "SPC")
        ]
      in letters
      ++ motion

symForEvent :: Word8 -> Maybe String
symForEvent = (flip lookup) symEventAlist

-- | An HID Code
-- Primarily an Int which corresponds to the standard HID code,
-- but wrapped so that it can be Shown in a friendlier format.
data HIDCode = HIDCode Word8
instance Show HIDCode where
  show (HIDCode n)
    | isJust s    = fromJust s
    | otherwise = show n
    where s = symForEvent n

-- | An HID Event which is sent as the result of a Chord
-- Contains either the event code with any modifiers,
-- or the offset into the StringLocations table for multi-event
-- chords.
-- TODO: Multi-Event chords should be inlined and expanded as necessary
-- This should likely be consistently an array.
data Event
  = KeyCode {
    mods :: [HIDMod]
  , code :: HIDCode }
  | StringOffset Word8
  deriving (Show)

-- | A mapping of a Chord to a resulting Event
data ChordMapping = ChordMapping
  { chord :: Chord
  , event :: Event
  } deriving (Show)

-- | A reference to the byte offset of a StringContents entry.
-- Used as a lookup table to fetch by an offset with fixed sizes
-- into the dynamically sized StringContents.
-- TODO: Abstract away
type StringLocation = Word32
