{-|
Module      : Twiml.Config.Binary
Description : Device Binary Format Support for Twiddler Config 
Copyright   : (c) Matt Whipple, 2018
License     : MIT
Maintainer  : matt@mattwhipple.com
Stability   : experimental
Portability : non-portable (GHC extensions)

[De]seriailization for Twiddler Configuration
to/from the Binary format used by the device.

Many of these are straightforward but are written in
longer form to use the more expressive record construction
syntax. There is likely a way to be expressive using a
terser form which may be adopted after familiarity grows.
-}
module Twiml.Config.Binary
  ( getConfig
  ) where

import Twiml.Config
  ( Chord(..)
  , ChordMapping(..)
  , Config(..)
  , Event(..)
  , Header(..)
  , HIDCode(..)
  , HIDMod(..)
  , Note(..)
  , Options(..)
  , StringLocation
  , Thumb(..)
  )

import Data.Binary.Get
  ( isEmpty
  , getWord8
  , getWord16le
  , getWord32le

  , Get
  )

import Data.Word
  ( Word8
  , Word16
  )

import Data.Bits
  ( testBit
  )

-- * Top Level Config

-- | Deserialize the complete 'Config' from a provided file.
--
-- Mostly calls into component functions.
getConfig :: Get Config
getConfig = do
  (h, nc) <- getHeader
  cs <- getMappings nc

  -- Determine the number of StringLocations based on the max referenced offset 
  let maxLoc = maxEvent (map event cs)
  slocs <- getStringLocations (maxLoc+1)
  sc <- getStringContentses
  return $! Config h cs slocs sc

-- | Return the max value for a 'StringOffset' type of event.
--
-- Use constructor matching to implicitly only test 'StringOffset' events
-- and to destructure such events.
maxEvent :: [Event] -> Word8
maxEvent es = foldr maxer 0 es
  where maxer (StringOffset o) a = max o a
        maxer _ a = a

-- * Header

-- | Deserialize the 'Header' and number of chord mappings from the binary file.
getHeader :: Get (Header, Word16)
getHeader = do
  v <- getWord8
  o <- getOptions
  cn <- getWord16le
  st <- getWord16le
  lc <- getWord16le
  rc <- getWord16le
  mc <- getWord16le
  maf <- getWord8
  krd <- getWord8
  _ <- getWord16le -- ^Unused/Reserved Word that will is discarded.
  let h = Header {
      version                 = v
    , options                 = o
    , sleepTimeout            = st
    , mouseLeftClickOrChord   = lc
    , mouseRightClickOrChord  = rc
    , mouseMiddleClickOrChord = mc
    , mouseAccelFactor        = maf
    , keyRepeatDelay          = krd
    }
  return (h, cn)

-- | Deserialize an 'Options' object from the bits in the next byte.
getOptions :: Get Options
getOptions = do
  bits <- getWord8
  let flagBit = testBit bits
  return Options {
    keyRepeatEnabled       = flagBit 0
  , directKeyModeEnabled   = flagBit 1
  , joystickMouseClickLeft = flagBit 2
  , bluetoothRadioDisabled = flagBit 3
  , stickNumKeyEnabled     = flagBit 4
  , stickShiftKeyEnabled   = flagBit 7
  }

---- ChordMappings ----

-- | Deserialize all the ChordMappings.
--
-- The header specifies how many mappings are present in the file,
-- this function accepts that number as the first parameter and
-- collects until it reaches 0.
-- TODO: nit there's likely a standard function to avoid the manual recursion
getMappings :: Word16 -> Get [ChordMapping]
getMappings 0 = return []
getMappings n = do
  cm <- getMapping
  ms <- getMappings (n-1)
  return (cm:ms)

-- | Deserialize a single ChordMapping.
getMapping :: Get ChordMapping
getMapping = ChordMapping <$> getChord <*> getEvent

-- | Deserialize a single Chord.
--
-- A Chord is represented in a single Word where each
-- bit indicates a possibly pressed button.
-- As indicated in the docs for Chord:
-- multiple thumb modifiers may be pressed, but each
-- row can only have one pressed Note. This is represented
-- in the Word consisting of what amount to bit triplets
-- where at most one bit will be set.
getChord :: Get Chord
getChord = do
  bits <- getWord16le
  let pn = parseNote bits
  return $! Chord (parseThumbs bits) (pn 1) (pn 5) (pn 9) (pn 13)    

-- | Deserialize Thumb modifiers.
--
-- Thumb modifiers are packed into a Chord Word interspersed between
-- the row triplets. Parsing the modifiers requires testing
-- each of those bits and collecting the associated modifiers
-- for set bits into a list.
parseThumbs :: Word16 -> [Thumb]
parseThumbs input =
     withBit 0  [N]
  ++ withBit 4  [A]
  ++ withBit 8  [C]
  ++ withBit 12 [S]
  where withBit = testBitList $ fromIntegral input

-- | Deserialize a single Note.
--
-- Each Note is represented by three contiguous bits
-- out of which at most one should be set. Each bit corresponds to
-- a Note and the provided offset determines the row.
parseNote :: Word16 -> Int -> Note
parseNote input offset
  | withBit 0 = R
  | withBit 1 = M
  | withBit 2 = L
  | otherwise = O
  where withBit = testBit input . (offset +)

-- | Deserialize an Event.
--
-- A single HIDEvent is represented inline,
-- while a composite event is an offset into the StringLocation table
getEvent :: Get Event
getEvent = do
  m <- getWord8
  key <- getWord8
  return $! if m == 255
            then StringOffset key
            else KeyCode (parseHIDMods m) (HIDCode key)

-- | Deserialize HIDMods.
--
-- HIDMods are a simple bitset so this results in creating
-- a list of the values associated with each set bit.
parseHIDMods :: Word8 -> [HIDMod]
parseHIDMods i =
     withBit 0 [LC]
  ++ withBit 1 [LS]
  ++ withBit 2 [LA]
  ++ withBit 3 [LG]
  ++ withBit 4 [RC]
  ++ withBit 5 [RS]
  ++ withBit 6 [RA]
  ++ withBit 7 [RG]
  where withBit = testBitList . fromIntegral $ i

---- String Tables ----

-- | Deserialize the given number of StringLocations
--
-- The number is determined from the referenced offsets,
-- the locations themselves are just references into the
-- contents table.
getStringLocations :: Word8 -> Get [StringLocation]
-- TODO: This should almost certainly be a single line
-- ..but locations as a standalone object should also disappear
getStringLocations 0 = return []
getStringLocations i = do
  l <- getWord32le
  rest <- getStringLocations (i-1)
  return $! l:rest

-- | Deserialize 'StringContents'es through to the end fo the file.
getStringContentses :: Get [[Event]]
getStringContentses = do
  empty <- isEmpty
  if empty
    then return []
    else do contents <- getStringContents
            contentses <- getStringContentses
            return (contents:contentses)

-- | Deserialize a single StringContents.
--
-- Each entry starts with a Word which indicates the size of the contents in bytes,
-- and that size informs the rest of the parsing.
getStringContents :: Get [Event]
getStringContents = do
  size <- getWord16le
  getEvents (fromIntegral size-2)
--  return $! StringContents (fromIntegral size) chords

-- Deserialize the multiple Events that are represented as a StringContents.
--
-- Each Event has one Byte for modifiers and one Byte for the code,
-- so each iteration decreases the remaining bytes by two.
getEvents :: Int -> Get [Event]
getEvents 0 = return $! []
getEvents n = do
  e <- getEvent
  es <- getEvents (n-2)
  return $! (e:es)

---- Utility Methods ----

-- | Return a list based on whether a bit is set in the provided input.
--
-- If 'byte' is set in 'input' then return 'v', otherwise return an empty list.
-- Calls to this can be composed using ++ to collect a list based on a bit set
testBitList :: Int -> Int -> [a] -> [a]
testBitList input byte v
  | set       = v
  | otherwise = []
  where
    set = testBit input byte
