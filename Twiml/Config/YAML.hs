{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Twiml.Config.YAML
  (
    Config(..)
  , Header(..)
  ) where

import Control.Applicative (empty)
import Twiml.Config
import Data.Aeson hiding (Options)
import Data.Aeson.TH (deriveJSON, defaultOptions)
import Data.Scientific

$(deriveJSON defaultOptions ''Config)
$(deriveJSON defaultOptions ''Header)
$(deriveJSON defaultOptions ''Options)
$(deriveJSON defaultOptions ''ChordMapping)

instance ToJSON Chord where
  toJSON (Chord mod n1 n2 n3 n4) = object
    [ "modifier"   .= mod
    , "row_1"      .= n1
    , "row_2"      .= n2
    , "row_3"      .= n3
    , "row_4"      .= n4
    ]

instance FromJSON Chord where
  parseJSON (Object v) = Chord
    <$> v .: "mod"
    <*> v .: "n1"
    <*> v .: "n2"
    <*> v .: "n3"
    <*> v .: "n4"

instance ToJSON Event where
  toJSON (KeyCode mods code) = object
    [ "mods" .= mods
    , "code" .= (show code)
    ]
  toJSON (StringOffset int) = Number (scientific (fromIntegral int) 0)

instance FromJSON Event where
  parseJSON (Number i) = return $! StringOffset (intVal (floatingOrInteger i))
    where intVal (Right i) = i
          intVal (Left f) = 0
  parseJSON (Object v) = KeyCode
    <$> v .: "mods"
    <*> v .: "code"

instance ToJSON Thumb where
  toJSON N = "N"
  toJSON A = "A"
  toJSON C = "C"
  toJSON S = "S"

instance FromJSON Thumb where
  parseJSON (String "N") = return $! N
  parseJSON (String "A") = return $! A
  parseJSON (String "C") = return $! C
  parseJSON (String "S") = return $! S 

$(deriveJSON defaultOptions ''Note)

instance ToJSON HIDMod where
  toJSON LC = "LC"
  toJSON LS = "LS"
  toJSON LA = "LA"
  toJSON LG = "LG"
  toJSON RC = "RC"
  toJSON RS = "RS"
  toJSON RA = "RA"
  toJSON LG = "RG"
instance FromJSON HIDMod where
  parseJSON (String "LC") = return $! LC
  parseJSON (String "LS") = return $! LS  
  parseJSON (String "LA") = return $! LA


instance ToJSON HIDCode where
  toJSON (HIDCode s) = Number (scientific (fromIntegral s) 0)
instance FromJSON HIDCode where
  parseJSON (Number n) = return $! HIDCode
    (intVal (floatingOrInteger n))
    where intVal (Right i) = i
          intVal (Left f) = 0
